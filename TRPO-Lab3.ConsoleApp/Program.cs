﻿using System;
using static TRPO_Lab3.Lib.CylinderArea;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------------\n TRPO_LAB_3\n------------");
            Console.WriteLine();

            Console.WriteLine("Введите значение радиуса цилиндра R:");
            double R = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение высоты цилиндра H:");
            double H = Double.Parse(Console.ReadLine());

            var result = GetCylinderArea(R, H);

            Console.WriteLine($"Площадь боковой поверхности круглого цилиндра равна: {result}");

            Console.ReadKey();
        }
    }
}
